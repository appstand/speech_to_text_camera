import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:speech_to_text_camera_view/speech_to_text_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder(
          future: availableCameras(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final CameraDescription frontCamera =
                  (snapshot.data as List<CameraDescription>).firstWhere(
                      (CameraDescription element) =>
                          element.lensDirection == CameraLensDirection.front);
              return Center(
                  child: FlatButton(
                child: Text("Open speech to text"),
                onPressed: () async {
                  SpeechToTextCameraResult result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              SpeechToTextScreen(frontCamera)));
                  if(result!=null){
                    print(result.toString());
                  }
                },
              ));
            } else {
              return Container();
            }
          }),
    );
  }
}
